﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Lab_2.Services;
using Lab_2.Views;

namespace Lab_2
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
